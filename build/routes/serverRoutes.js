"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const serverControllers_1 = require("../controllers/serverControllers");
class ServerRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get("/", serverControllers_1.serverController.server);
    }
}
const serverRoutes = new ServerRoutes();
exports.default = serverRoutes.router;
