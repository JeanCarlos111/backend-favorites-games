import { Router } from "express";
import { serverController } from "../controllers/serverControllers"

class ServerRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  config(): void {
    this.router.get("/", serverController.server);
  }
}

const serverRoutes = new ServerRoutes();

export default serverRoutes.router;
