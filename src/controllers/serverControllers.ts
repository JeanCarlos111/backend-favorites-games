import { Request, Response } from "express";

class ServerController {
  public server(req: Request, res: Response) {
    res.send("Works fine");
  }
}

export const serverController = new ServerController();
