import { Request, Response } from "express";

import db from "../database";

class GamesController {
  async read(req: Request, res: Response) : Promise<void> {
    await db.query("SELECT * FROM games", (error, response) => {
      if (error) throw error;
      res.json(response);
    });
  }
  async readById(req: Request, res: Response) : Promise<any> {
    const { id } = req.params;
    await db.query(
      "SELECT * FROM games WHERE id = ?",
      [id],
      (error, response) => {
        if (response.length > 0) {
          return res.json(response[0]);
        }
        res.status(404).json({ message: "The game doesn't exist" });
      }
    );
  }
  async create(req: Request, res: Response) : Promise<void>  {
    const result = await db.query("INSERT INTO games set ?", [req.body]);
    res.json({ message: "Game was created!" });
  }
  async update(req: Request, res: Response) : Promise<void> {
    const { id } = req.params;
    const oldGame = req.body;
    await db.query("UPDATE games set ? WHERE id = ?", [req.body, id]);
    res.json({ message: `"The game ${id} was updated successful` });
  }
  async delete(req: Request, res: Response) : Promise<void> {
    const { id } = req.params;
    await db.query("DELETE FROM games WHERE id = ?", [id]);
    res.json({ message: `"The game ${id} was deleted successful"` });
  }
}

export const gamesController = new GamesController();
